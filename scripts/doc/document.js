/**
 * JS um ein Hamburger-Menu zu realisieren
 */

let mobile = document.createElement('div');
mobile.className = 'nav-mobile';
let temp = document.querySelector(".nav");
temp.appendChild(mobile);

let mobileNav = document.querySelector('.nav-mobile');
let toggle = document.querySelector('.nav-list');
mobileNav.onclick = function () {
    toggleClass(this, 'nav-mobile-open');
    toggleClass(toggle, 'nav-active');
};


/**
 * has class
 * @param elem
 * @param className
 * @returns {boolean}
 */
function hasClass(elem, className) {
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}


/**
 * toggleClass
 * @param elem
 * @param className
 */
function toggleClass(elem, className) {
    let newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }
        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    } else {
        elem.className += ' ' + className;
    }
}
