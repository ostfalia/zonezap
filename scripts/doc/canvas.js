"use strict";
// ECMA 6

let anzahl = 10; // Anzahl der Kreise die gebildet werden

let canvasLayer0 = document.getElementById('layer0');
canvasLayer0.width = document.getElementById('fstcanvtest').offsetWidth;
canvasLayer0.height = document.getElementById('fstcanvtest').offsetHeight;

let ctxLayer0 = canvasLayer0.getContext('2d');

let water = [
    'rgba(0, 48, 90, x)',
    'rgba(0, 75, 141, x)',
    'rgba(0, 116, 217, x)',
    'rgba(65, 147, 217, x)',
    'rgba(122, 186, 242, x)'
];

let colors = water;
let circleArray = [];
let customCircleArray = [];

function Circle(x, y, dx, dy, radius, color, fill, alpha, layer) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.color = color;
    this.fill = fill; // bool
    this.alpha = alpha; // float 0 - 1
    this.layer = layer; // object


    this.draw = function () {
        this.layer.beginPath();
        this.layer.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        if (fill) {
            this.layer.fillStyle = this.color.replace('x', '' + this.alpha);
            this.layer.fill();
        } else {
            this.layer.strokeStyle = "gold";
            this.layer.stroke();
        }
    };

    this.chgXdirection = function () {
        this.dx = -this.dx;
    };

    this.chgYdirection = function () {
        this.dy = -this.dy;
    };

    this.update = function () {
        if (this.x + this.radius > canvasLayer0.width || this.x - this.radius < 0)
            this.chgXdirection();

        if (this.y + this.radius > canvasLayer0.height || this.y - this.radius < 0)
            this.chgYdirection();

        this.x += this.dx;
        this.y += this.dy;

        this.draw();
    }
}


for (let i = 0; i < anzahl; i++) {
    let radius = Math.random() * 60;
    let x = Math.random() * (canvasLayer0.width - radius * 2) + radius;
    let dx = (Math.random() - 0.5) * 4; // +- random
    let y = Math.random() * (canvasLayer0.height - radius * 2 + radius);
    let dy = (Math.random() - 0.5) * 4;
    let color = colors[Math.floor(Math.random() * colors.length)];
    let fill = true;
    let alpha = Math.random();
    circleArray.push(new Circle(x, y, dx, dy, radius, color, fill, alpha, ctxLayer0));
}

function getDistance(x1, y1, x2, y2) {
    let xDistance = x2 - x1;
    let yDistance = y2 - y1;

    return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
}

function animate() {
    // nur Layer 0 soll animiert werden

    // dem Browser mitteilen, dass die Animation vor dem nächsten repaint aktualisiert werden soll - rekursiver Aufruf
    requestAnimationFrame(animate);

    // canvas (Layer 0) löschen und zu animierende Objekte neu Zeichnen
    ctxLayer0.clearRect(0, 0, canvasLayer0.width, canvasLayer0.height);

    for (let i = 0; i < circleArray.length; i++) {
        // prüfen ob Kollision mit einem Benutzerkreis
        if (customCircleArray !== undefined && customCircleArray.length > 0) {
            for (let k = 0; k < customCircleArray.length; k++) {
                if (getDistance(circleArray[i].x, circleArray[i].y, customCircleArray[k].x, customCircleArray[k].y) < circleArray[i].radius + customCircleArray[k].radius) {
                    circleArray[i].chgXdirection();
                    circleArray[i].chgYdirection();
                }
            }
        }
        // Kreis mit geänderten Koordinaten zeichnen
        circleArray[i].update();
    }
}


// // Benutzerkreis per Mausklick hinzufügen
// canvasLayer1.addEventListener('click', function (e) {
//     // @ToDo: x/y nach scrolling falsch! wie kann man das abfangen?
//     customCircleArray.push(new Circle(e.x, e.y, 0, 0, 30, 'gold', true, Math.random(), ctxLayer1));
//     customCircleArray[customCircleArray.length-1].draw();
// });
// // Benutzerkreis per Touch hinzufügen
// canvasLayer1.addEventListener('touchstart', function (e) {
//     // www.homeandlearn.co.uk/JS/html5_canvas_touch_events.html
//
//     // gegenüber click-Listener müssen hier zunächst die Standard-Touch Events unterdrückt werden;
//     // das bedeutet auch, dass zunächst keine Wischgesten mehr unterstützt werden, solange sie nicht mit den
//     // anderen Events behandelt werden...
//     event.preventDefault();
//     customCircleArray.push(new Circle(event.targetTouches[0].pageX, event.targetTouches[0].pageY, 0, 0, 30, 'red', true, 1, ctxLayer1));
//     customCircleArray[customCircleArray.length-1].draw();
// });

animate();

// Infos anzeigen
// ctxLayer1.textBaseline = "top";
// ctxLayer1.textAlign = "left";
// ctxLayer1.font = "12pt Arial";
// ctxLayer1.fillStyle = "gold";
// ctxLayer1.fillText("LU (0,0)", 0, 0);
//
// ctxLayer1.textBaseline = "top";
// ctxLayer1.textAlign = "right";
// ctxLayer1.font = "12pt Arial";
// ctxLayer1.fillStyle = "gold";
// ctxLayer1.fillText("RU ("+canvasLayer1.width+",0)", canvasLayer1.width, 0);
//
// ctxLayer1.textBaseline = "bottom";
// ctxLayer1.textAlign = "left";
// ctxLayer1.font = "12pt Arial";
// ctxLayer1.fillStyle = "gold";
// ctxLayer1.fillText("LB (0,"+canvasLayer1.height+")", 0, canvasLayer1.height);
//
// ctxLayer1.textBaseline = "bottom";
// ctxLayer1.textAlign = "right";
// ctxLayer1.font = "12pt Arial";
// ctxLayer1.fillStyle = "gold";
// ctxLayer1.fillText("RB ("+canvasLayer1.width+","+canvasLayer1.height+")", canvasLayer1.width, canvasLayer1.height);
