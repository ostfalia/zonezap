"use strict";

let backgroundItem = document.getElementById("background");
let backgroundCanvas = backgroundItem.getContext("2d");

backgroundItem.style.webkitFilter = "blur(5px)";

backgroundItem.width = window.innerWidth;
backgroundItem.height = window.innerHeight;


let images = new Image();
images.src = "images/game/wolke.svg";


class Cloud {

    constructor(starty, startx, speed, size) {
        // this.starty = starty;
        this.y = starty;
        this.startx = startx;
        this.speed = speed;
        this.size = size;
    }

    draw() {
        this.y = this.y + this.speed;
        if (this.y > window.innerWidth) {
            this.y = 0 - images.width * this.size;
        }
        backgroundCanvas.drawImage(images, this.y, this.startx, images.width * this.size, images.height * this.size);
    }
}


let clouds = [
    new Cloud(0, 40, 0.2, 0.3),
    new Cloud(0, 100, 0.3, 0.6),
    new Cloud(-100, 100, 0.3, 0.6),
    new Cloud(-200, 530, 0.2, 0.4),
    new Cloud(400, 300, 0.3, 0.6),
    new Cloud(900, 400, 0.3, 0.6),
    new Cloud(1200, 100, 0.3, 0.6),

];

function update() {

    // nur bei einer bestimmten Größe soll der animierte Hintergrund angezeigt werden (um bei mobilen Geräten Performance zu sparen)
    if (isDesktop()){
        backgroundCanvas.clearRect(0, 0, backgroundItem.width, backgroundItem.height);

        for (let i = 0; i < clouds.length; i++) {
            clouds[i].draw();
        }

        window.requestAnimationFrame(update);

    }
}

function isDesktop(){
    // https://css-tricks.com/snippets/css/media-queries-for-standard-devices/
    return backgroundItem.width > 1024;
}

update();