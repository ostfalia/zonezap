# README #

Dies ist die offizielle README Datei des Zonezap Projekts.

### Allgemein ###

* Zonezap, ein Kachel basierendes Labyrinth Browser Spiel
* Version: 1.0
* [Bitbucket Projektseite](https://bitbucket.org/ostfalia/zonezap/overview)
* [gehostete Projektseite](http://wp.uni.lederich.de/)

### Verwendung ###

* das Spiel beruht auf JavaScript und HTML5 und kann direkt aus diesem Verzeichnis geöffnet werden
  * index.html (Projektseite)
  * game.html (Das Browser-Spiel)
* vorhandene Cheats zum Testen der Levels
  * game.html/?cheat=unlimitedlives (Leben werden beim Ableben nicht abgezogen)
  * game.html/?cheat=ghostmode (der Spieler läuft durch die Gegner; ein Kreis beim Spieler zeigt diesen Modus an)

### Entwicklung ###

#### Verfahrensweise ####

* jede Entwicklung erfolgt in einem eigenen Branch
* für das git/branch handling wird der git-flow Prozess wie in [Getting Started - Git-Flow (Jaco van Staden)](https://yakiloo.com/getting-started-git-flow/) beschrieben verwendet:
  * `master´ - enthält nur die veröffentlichten Tags
  * `develop´ - hiervon ziehen sich die Entwickler eine Arbeitskopie zur Entwicklung; hiervon gehen auch andere Branches (feature-branch, sofern es offizielle Branches für benannte features gibt und den release-Branch)
* ein merge in den master-Branch erfolgt ausschließlich mittels Pull-Requests, mit einem u.g. Ansprechpartner als Reviewer
* (eigene) nicht mehr benötigte Branches (nach Merge) löschen

#### git ####

1. Main Repo klonen: `git clone https://bitbucket.org/ostfalia/zonezap.git`
1. Einen lokalen Branch für die Entwicklung erstellen: `git checkout -b my-branch`
1. Änderungen im Arbeitsbranch lokal hinzufügen: `git commit -m "Erläuterungen zu den Änderungen"`
1. Branch nach Bitbucket übertragen: `git push -u origin my-branch`
1. Auf der Bitbucket Seite einen Pull-Request erstellen

Alternativ können die Schritte weitgehendst auch mit der IDE vorgenommen werden. Unter 
WebStorm kann auch ein git Projekt wie folgt angelegt werden - im Menü: 
* `CS | Git | clone...`
* dort als Repo-Url entsprechend: `https://bitbucket.org/ostfalia/zonezap.git`

#### Fehlerbehandlung ####
* Sichere setTimeout, Dateihandling oder Daten aus Benutzereingaben per try/catch ab
* per window.onerror werden ansonsten aktuell alle Fehler 'aufgefangen' die nicht 
per try/catch behandelt wurden
* Requests (wie bei loadJSON) mit der Fehler-Funktion `showErr()` im Browser anzeigen lassen

### Ansprechpartner/ Verantwortliche ###

* [Simon Rindermann](mailto:s.rindermann@ostfalia.de)
* [Dennis Lederich](mailto:d.lederich@ostfalia.de)